from django.core.management.base import BaseCommand, CommandError
from map.models import Fires, GeoPolygon, DecisionTable, Meteocondition, Fires2FireWorked, FireWorked, Fires2GeoPolygon, Attributes
from random import randint
from django.db.models import Avg, Max, Min
from decimal import Decimal
from decimal import *



class Command(BaseCommand):

    def handle(self, *args, **options):
        decisions = DecisionTable.objects.all()

#condition attributes

        min = decisions.aggregate(m=Min('crowning_square'))
        min = min['m']
        max = decisions.aggregate(n=Max('crowning_square'))
        max = max['n']
        h_crowning_square = (max - min) / 4
        print(h_crowning_square)

        min = decisions.aggregate(m=Min('ground_square'))
        min = min['m']
        max = decisions.aggregate(n=Max('ground_square'))
        max = max['n']
        h_ground_square = (max - min) / 4
        print(h_ground_square)

        min = decisions.aggregate(m=Min('unforest_square'))
        min = min['m']
        max = decisions.aggregate(n=Max('unforest_square'))
        max = max['n']
        h_unforest_square = (max - min) / 4
        print(h_unforest_square)

        min = decisions.aggregate(m=Min('wood_volume_per_ha'))
        min = min['m']
        max = decisions.aggregate(n=Max('wood_volume_per_ha'))
        max = max['n']
        h_wood_volume_per_ha = (max - min) / 4
        print(h_wood_volume_per_ha)





# decision attributes
#        min = decisions.aggregate(m=Min('crowning_square'))
        min = decisions.aggregate(Min('crowning_square'))
        print(min)
        max = decisions.aggregate(Max('crowning_square'))
        print(max)
        h = (max - min) / 4
        print(h)


#        for d in  DecisionTable.objects.all():
 #           fi = d.fire_id
#            f = Fires2GeoPolygon.objects.filter(fire_id=fi)
#            if f.count() >= 1:
#                k = f[0].kvartal
#                v = f[0].vydel
#            g = GeoPolygon.objects.filter(kvartal=k, vydel=v)
#            if g.count() > 0:
            #    print "Hi"
#                w = g[0].wood_volume_per_ha
#                d.wood_volume_per_ha = w;
#                d.save()

#            m = Meteocondition.objects.filter(curdate=da)
#            if m.count()>0:
#                print "Hi"
#                p = m[0].pjc
#                print(p)
#                pr = m[0].pr
#                print(pr)
#            else:
#                p = m.pjc
#                print(p)
#                pr = m.pr
#                print(pr)
#            d.pjc = p
#            d.pr = pr
#            d.save()

        min = decisions.aggregate(m=Min('forestry_man_days'))
        min = min['m']
        max = decisions.aggregate(n=Max('forestry_man_days'))
        max = max['n']
        h_forestry_man_days = (max - min) / 4
        print(h_forestry_man_days)



        min = decisions.aggregate(m=Min('emergency_man_days'))
        min = min['m']
        max = decisions.aggregate(n=Max('emergency_man_days'))
        max = max['n']
        h_emergency_man_days = (max - min) / 4
        print(h_emergency_man_days)


        min = decisions.aggregate(m=Min('another_man_days'))
        min = min['m']
        max = decisions.aggregate(n=Max('another_man_days'))
        max = max['n']
        h_another_man_days = (max - min) / 4
        print(h_another_man_days)


        min = decisions.aggregate(m=Min('forestry_fire_eng'))
        min = min['m']
        max = decisions.aggregate(n=Max('forestry_fire_eng'))
        max = max['n']
        h_forestry_fire_eng = (max - min) / 4
        print(h_forestry_fire_eng)


        min = decisions.aggregate(m=Min('forestry_another'))
        min = min['m']
        max = decisions.aggregate(n=Max('forestry_another'))
        max = max['n']
        h_forestry_another = (max - min) / 4
        print(h_forestry_another)

        min = decisions.aggregate(m=Min('emergency_fire_eng'))
        min = min['m']
        max = decisions.aggregate(n=Max('emergency_fire_eng'))
        max = max['n']
        h_emergency_fire_eng = (max - min) / 4
        print(h_emergency_fire_eng)



        min = decisions.aggregate(m=Min('emergency_another'))
        min = min['m']
        max = decisions.aggregate(n=Max('emergency_another'))
        max = max['n']
        h_emergency_another = (max - min) / 4
        print(h_emergency_another)


        min = decisions.aggregate(m=Min('another_fire_eng'))
        min = min['m']
        max = decisions.aggregate(n=Max('another_fire_eng'))
        max = max['n']
        h_another_fire_eng = (max - min) / 4
        print(h_another_fire_eng)


        min = decisions.aggregate(m=Min('another_another'))
        min = min['m']
        max = decisions.aggregate(n=Max('another_another'))
        max = max['n']
        h_another_another = (max - min) / 4
        print(h_another_another)


        min = decisions.aggregate(m=Min('total_fire_eng'))
        min = min['m']
        max = decisions.aggregate(n=Max('total_fire_eng'))
        max = max['n']
        h_total_fire_eng = (max - min) / 4
        print(h_total_fire_eng)

        min = decisions.aggregate(m=Min('total_another'))
        min = min['m']
        max = decisions.aggregate(n=Max('total_another'))
        max = max['n']
        h_total_another = (max - min) / 4
        print(h_total_another)

#            print da



#            print(q)
#            print(cr)
#            print(gr)



#            g.firerisk = q
#            g.save()


#        geopolygons = GeoPolygon.objects.all()
 #       l = geopolygons.aggregate(m=Min('firerisk'))
#        print(l)
#        h = geopolygons.aggregate(ma=Max('firerisk'))
#        print(h)

#        a = int(l['m'])
#        print(a)
#        b = int(h['ma'])
#        print(b)
#        d = b - a
#        print(d)
        
        #the number of classes
#        n = 4

 #       print(n)

  #      h = d / n
   #     print(h)

    #    i = 0
     #   aa = []
      #  aa.append(0)

#        while i < n:
 #           print i
  #          i += 1
   #         q = aa[i-1] + h
    #        aa.append(q)
     #       print(aa[i])

# fire danger


      #  pr =  0.1
#fire risk for all regions

       # for g in GeoPolygon.objects.all():
        #    zzz = Decimal(g.wood_volume_per_ha)
         #   prz = Decimal(pr)
##            qq = g.wood_volume_per_ha * pr
#            qq =  zzz * prz
#            print(qq)
#            g.firerisk = qq
#            g.save()


        a = Attributes()
       
 #       a.name = "crowning_square"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_crowning_square
#        a.bound3 = a.bound2 + h_crowning_square
#        a.bound4 = a.bound3 + h_crowning_square
#        a.bound5 = a.bound4 + h_crowning_square
     
 #       a.name = "ground_square"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_ground_square
#        a.bound3 = a.bound2 + h_ground_square
#        a.bound4 = a.bound3 + h_ground_square
#        a.bound5 = a.bound4 + h_ground_square
     
#        a.name = "unforest_square"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_unforest_square
#        a.bound3 = a.bound2 + h_unforest_square
#        a.bound4 = a.bound3 + h_unforest_square
#        a.bound5 = a.bound4 + h_unforest_square
     
#        a.name = "wood_volume_per_ha"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_wood_volume_per_ha
#        a.bound3 = a.bound2 + h_wood_volume_per_ha
#        a.bound4 = a.bound3 + h_wood_volume_per_ha
#        a.bound5 = a.bound4 + h_wood_volume_per_ha
     
#        a.name = "pjc"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + 0.25
#        a.bound3 = a.bound2 + 0.25
#        a.bound4 = a.bound3 + 0.25
#        a.bound5 = a.bound4 + 0.25
     
#        a.name = "pr"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + 0.25
#        a.bound3 = a.bound2 + 0.25
#        a.bound4 = a.bound3 + 0.25
#        a.bound5 = a.bound4 + 0.25
     
     
     
#        a.name = "forestry_man_days"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_forestry_man_days
#        a.bound3 = a.bound2 + h_forestry_man_days
#        a.bound4 = a.bound3 + h_forestry_man_days
#        a.bound5 = a.bound4 + h_forestry_man_days
     
 #       a.name = "emergency_man_days"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_emergency_man_days
#        a.bound3 = a.bound2 + h_emergency_man_days
#        a.bound4 = a.bound3 + h_emergency_man_days
#        a.bound5 = a.bound4 + h_emergency_man_days
     
  

#        a.name = "another_man_days"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_another_man_days
#        a.bound3 = a.bound2 + h_another_man_days
#        a.bound4 = a.bound3 + h_another_man_days
#        a.bound5 = a.bound4 + h_another_man_days
     
  

#        a.name = "forestry_fire_eng"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_forestry_fire_eng
#        a.bound3 = a.bound2 + h_forestry_fire_eng
#        a.bound4 = a.bound3 + h_forestry_fire_eng
#        a.bound5 = a.bound4 + h_forestry_fire_eng
     
  

#        a.name = "forestry_another"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_forestry_another
#        a.bound3 = a.bound2 + h_forestry_another
#        a.bound4 = a.bound3 + h_forestry_another
#        a.bound5 = a.bound4 + h_forestry_another
     
  
#        a.name = "emergency_another"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_emergency_another
#        a.bound3 = a.bound2 + h_emergency_another
#        a.bound4 = a.bound3 + h_emergency_another
#        a.bound5 = a.bound4 + h_emergency_another
     
  
#        a.name = "another_fire_eng"
#        a.bound1 = 0
 #       a.bound2 = a.bound1 + h_another_fire_eng
#        a.bound3 = a.bound2 + h_another_fire_eng
#        a.bound4 = a.bound3 + h_another_fire_eng
#        a.bound5 = a.bound4 + h_another_fire_eng
     
  
#        a.name = "another_another"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_another_another
#        a.bound3 = a.bound2 + h_another_another
#        a.bound4 = a.bound3 + h_another_another
#        a.bound5 = a.bound4 + h_another_another
     
  
#        a.name = "total_fire_eng"
#        a.bound1 = 0
#        a.bound2 = a.bound1 + h_total_fire_eng
#        a.bound3 = a.bound2 + h_total_fire_eng
#        a.bound4 = a.bound3 + h_total_fire_eng
#        a.bound5 = a.bound4 + h_total_fire_eng
     
  
        a.name = "total_another"
        a.bound1 = 0
        a.bound2 = a.bound1 + h_total_another
        a.bound3 = a.bound2 + h_total_another
        a.bound4 = a.bound3 + h_total_another
        a.bound5 = a.bound4 + h_total_another
     
  



        a.save()




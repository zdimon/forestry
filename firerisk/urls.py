
from django.conf.urls import patterns, include, url
from django.contrib.auth.decorators import login_required
urlpatterns = patterns('',
   url(r'^index/(?P<risk>\w+)$', 'firerisk.views.index', name='firerisk-index')
)

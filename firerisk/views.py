from django.template import loader, RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response, get_object_or_404

def index(request,risk):
    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['0', '#90EE90'],
          ['0-66', '#FFAEB9'],
          ['66-132', '#FA8072'],
          ['132 - 198', '#FF4040'],
          ['> 198', '#FF0000']]
    context = {'base_url': base_url, 'risk': risk, 'legend': legend}
    return render_to_response('index.html', context, RequestContext(request))

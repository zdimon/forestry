# -*- coding: utf-8 -*- 
import os
from django.core.management.base import BaseCommand, CommandError
from map.models import Meteocondition, Nesterov
class Command(BaseCommand):
    def handle(self, *args, **options) :
        os.system('cls' if os.name == 'nt' else 'clear');z_pas=1;pl_cnt=1;psrc_cnt=1;p_inv=1
        new_style=0 #new_style=страничный вывод(1)или(0)сразу по ходу обработки строк
        dwr=0;idcnt=1;lin_cnt=0;pas_cnt=0;inv_cnt=0;neg_cnt=0;sp=" | ";global rL;rL=[];global eL;eL=[];#инициализация
        help="Возможные команды:\nup   - на страницу вверх\ndown - на страницу вниз\nexit - завершить работу скрипта\n"
        prim="Примечание: для правильного отображения, ширина\n            консоли должна быть не менее 60симв."
        global max_id;max_id=str(Meteocondition.objects.all().count());LPP=20; # linePerPage                  
        for g in Meteocondition.objects.all() : 
            d = Nesterov();idcnt+=1;dwr+=1;kNes=0.0;# выбор таб.,счет обраб-ных,дней без дождя,сброс коэф.Нест.
            if g.curdate == None or g.t == None or g.r == None or g.rrr == None : 
                if p_inv:   # !undl=""!возможно не нужно иниц перем
                    lin_cnt+=1;undl=""#сделать еще когда будет постраничный вывод что бы сверху шапка была
                    ptl="| "+adds(idcnt,len(max_id)," ")   #номер строки
                    ptl+=sp+adds(g.curdate,      10," ")   #дата
                    ptl+=sp+"t= "+adds(g.t,       6," ")   #темпер
                    ptl+=sp+"r= "+adds(g.r,       6," ")   #т.росы
                    ptl+=sp+"rrr= "+adds(g.rrr,6," ")+" |" #осадки нужно +1 поправку к макс.цифр делать ввиду минуса
                    undl=len(ptl)*"*"                      #влияет ли минус на максимальное кол-во знаков в поле таб
                    if new_style: rL.append(undl);rL.append(ptl);rL.append(undl);#вывод постранично (новый вид)
                    else        : print undl+"\n"+ptl+"\n"+undl                  #вывод всех строк (старый вид)
                    #print 'text', запятая в конце принта означает-не будет перехода на новую строку
                inv_cnt+=1;continue
            if g.rrr > 3 : # rrr - Кол-во осадков	
                cnt=0;                   
                while cnt<dwr : cnt+=1;kNes+=g.t*(g.t-g.r) #dwr-Days Without Rain|t-темп.воздуха|r-темп.т.росы
                dwr=0 
            if kNes<0.0 : kNes=0.0;neg_cnt+=1;Fdc="I"#может не считать минусовые значения if g.t<0 or g.r<0
            else        : Fdc=Fd(kNes)              
#откл запись#d.dt=g.curdate;d.knes=kNes;d.fdc=Fdc;d.save()  #запись в колонки и сохранение в таб.
            if kNes == 0.0 and z_pas: pas_cnt+=1 #пропускать ли пустые строки
            else : 
                lin_cnt+=1;
                ptl="| "+adds(idcnt,len(max_id)," ")      #номер строки
                ptl+=sp+adds(g.curdate,      10," ")      #дата                       
                ptl+=sp+adds(kNes,           11," ")      #коэф.Нест
                ptl+=sp+adds(Fdc,             3," ")      #кл.опас
                ptl+=sp+adds(g.t,             6," ")      #темпер
                ptl+=sp+adds(g.r,             6," ")+" |" #т.росы
                if new_style: rL.append(ptl);                     
                else        : print ptl
        psh ="| всего |";pcnt="| строк |";wt=9
        if pl_cnt      : psh+=" отображ |";pcnt+=" "+adds(lin_cnt,7," ")+" |";wt+=10 #всего,не считая звездочек
        if inv_cnt > 0 : psh+=" поврежд |";pcnt+=" "+adds(inv_cnt,7," ")+" |";wt+=10 #всего поврежденных строк
        if neg_cnt > 0 : psh+=" отрицат |";pcnt+=" "+adds(neg_cnt,7," ")+" |";wt+=10 #всего строк с минусовым значением
        if z_pas       : psh+=" игнорир |";pcnt+=" "+adds(pas_cnt,7," ")+" |";wt+=10 #всего пропущенно строк
        if psrc_cnt    : psh+=" обработ |";pcnt+=" "+adds(max_id, 7," ")+" |";wt+=10 #всего обработанно из источника
        razd=adds("",wt,"|")
        if new_style : eL.append(razd);eL.append(psh);eL.append(pcnt);eL.append(razd);eL.append(prim);
        else         : print razd+"\n"+psh+"\n"+pcnt+"\n"+razd+"\n"+prim;exit()
        global rzsh;rzsh=adds("",len(rL[1].decode('utf-8')),"=");
        lcnt=LPP;PgDw(lcnt-LPP,lcnt,len(rL[1].decode('utf-8')));eTab();
        while new_style :
            ans=raw_input("Введите команду: ")                            
            if ans=="down" or ans=="d" :   
                lcnt+=LPP;
                if lcnt<len(rL):PgDw(   lcnt-LPP,   lcnt,len(rL[1].decode('utf-8')));eTab();
                else           :PgDw(len(rL)-LPP,len(rL),len(rL[1].decode('utf-8')));eTab();lcnt =len(rL);
            elif ans == "up" or ans == "u" :   
                lcnt-=LPP;
                if lcnt-LPP > 0:PgUp(   lcnt-LPP,   lcnt,len(rL[1].decode('utf-8')));eTab();
                else           :PgUp(          0,    LPP,len(rL[1].decode('utf-8')));eTab();lcnt =LPP;
            elif ans == "exit" or ans == "e" : print "\n..::завершение работы скрипта::..\n";exit()  
            else                             : print help
def PgUp(home_id,end_id,wt) : #page up в качестве заглушки можно два раза прогонять эту функцию(костыль)  
    sztx=" строки с "+str(home_id)+" по "+str(end_id)+", всего "+str(len(rL))+" "
    szr=(wt-len(sztx.decode('utf-8')))/2
    sht=adds(adds("",szr,"=")+sztx,wt,"=")
    os.system('cls' if os.name == 'nt' else 'clear')
    print rzsh+"\n| "+adds("id",len(max_id)," ")+" |    дата    | к.Нестерова |кл.оп| темпер | т.росы |\n"+rzsh
    for res in rL[home_id:end_id]: print res
    print rzsh+"\n"+sht+"\n"+rzsh; print
def PgDw(home_id,end_id,wt) : #page down      clear #screen каждый раз при смене страници              
    sztx=" строки с "+str(home_id)+" по "+str(end_id)+", всего "+str(len(rL))+" "
    szr=(wt-len(sztx.decode('utf-8')))/2
    sht=adds(adds("",szr,"=")+sztx,wt,"=")
    os.system('cls' if os.name == 'nt' else 'clear')
    print rzsh+"\n| "+adds("id",len(max_id)," ")+" |    дата    | к.Нестерова |кл.оп| темпер | т.росы |\n"+rzsh
    for res in rL[home_id:end_id]: print res
    print rzsh+"\n"+sht+"\n"+rzsh; print
def eTab() :
    for err in eL : print err
def adds(txt,msm,sym) : return str(txt)+str((msm-len(str(txt).decode('utf-8') ))*sym)
def Fd(kN):
    if   kN <=   300.0 : return  "I" # отсутствует    	            
    elif kN <=  1000.0 : return "II" # малая	                
    elif kN <=  4000.0 : return"III" # средняя	            
    elif kN <= 10000.0 : return "IV" # высокая                    
    elif kN >  10000.0 : return  "V" # чрезвычайная

#* идея сделать view с результатами расчетов, для построения карты	
#* идея вертикального вывода как в mysql на случай если там будет много инфы и не будет помещятся на экран 
#  *******строка 1 **********
#  колонка 1 = (...)
#  колонка 2 = (...)
#  (...)
#  *******строка 2 **********
#  колонка 1 = (...)
#  колонка 2 = (...)
#  (...)

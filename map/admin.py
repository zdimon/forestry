
from django.contrib import admin
from map.models import ForestryGroup
from map.models import Forestry
from map.models import GeoKvartal
from map.models import TypePolygon
from map.models import GeoPolygon
from map.models import ForestElement
from map.models import ForestElement2GeoPolygon
from map.models import TypeValue
from map.models import TypeParamPolygon
from map.models import ParamValueSelect
from map.models import ValueParamPolygon
from map.models import FireDetection
from map.models import FireCause
from map.models import ExtingCosts
from map.models import FireDamage
from map.models import FireWorked
from map.models import Fires
from map.models import Fires2ExtingCosts
from map.models import Fires2FireDamage
from map.models import Fires2FireWorked
from map.models import Fires2GeoPolygon
from map.models import Meteocondition
from map.models import DecisionFinal
from map.models import DecisionWrong

from simple_translation.admin import TranslationAdmin
from django.contrib.gis import admin
from django.contrib.gis.admin import OSMGeoAdmin, GeoModelAdmin
from django.conf import settings

import django_databrowse
django_databrowse.site.register(GeoKvartal)

USE_GOOGLE_TERRAIN_TILES = False

class ForestryGroupAdmin(TranslationAdmin):
    pass
admin.site.register(ForestryGroup, ForestryGroupAdmin)

class ForestryAdmin(TranslationAdmin):
    pass
admin.site.register(Forestry, ForestryAdmin)

class GeoKvartalAdmin(OSMGeoAdmin):
    map_template = 'gis/admin/google.html'
    extra_js = ['http://openstreetmap.org/openlayers/OpenStreetMap.js', 'http://maps.google.com/maps?file=api&amp;v=2&amp;key=%s' % settings.GOOGLE_MAPS_API_KEY]


admin.site.register(GeoKvartal, GeoKvartalAdmin)

class TypePolygonAdmin(TranslationAdmin):
    list_display = ['__unicode__', 'fill_color', 'border_color', 'fill_color_rect', 'border_color_rect', 'is_pub']
    list_editable = ['is_pub', 'fill_color', 'border_color']
admin.site.register(TypePolygon, TypePolygonAdmin)

class GeoPolygonAdmin(OSMGeoAdmin):
    map_template = 'gis/admin/google.html'
    extra_js = ['http://openstreetmap.org/openlayers/OpenStreetMap.js', 'http://maps.google.com/maps?file=api&amp;v=2&amp;key=%s' % settings.GOOGLE_MAPS_API_KEY]
    list_display = ['__unicode__', 'wood_volume_per_ha']
    search_fields = ['=kvartal', '=vydel']
    list_editable = ['wood_volume_per_ha']


admin.site.register(GeoPolygon, GeoPolygonAdmin)

class ForestElementAdmin(TranslationAdmin):
    pass
admin.site.register(ForestElement, ForestElementAdmin)

class ForestElement2GeoPolygonAdmin(admin.ModelAdmin):
    pass
admin.site.register(ForestElement2GeoPolygon, ForestElement2GeoPolygonAdmin)

class TypeValueAdmin(admin.ModelAdmin):
    pass
admin.site.register(TypeValue, TypeValueAdmin)

class TypeParamPolygonAdmin(TranslationAdmin):
    pass
admin.site.register(TypeParamPolygon, TypeParamPolygonAdmin)

class ParamValueSelectAdmin(admin.ModelAdmin):
    pass
admin.site.register(ParamValueSelect, ParamValueSelectAdmin)




class ValueParamPolygonAdmin(admin.ModelAdmin):
    pass
admin.site.register(ValueParamPolygon, ValueParamPolygonAdmin)




class FireDetectionAdmin(TranslationAdmin):
    pass
admin.site.register(FireDetection, FireDetectionAdmin)

class FireCauseAdmin(TranslationAdmin):
    pass
admin.site.register(FireCause, FireCauseAdmin)

class ExtingCostsAdmin(TranslationAdmin):
    pass
admin.site.register(ExtingCosts, ExtingCostsAdmin)

class FireDamageAdmin(TranslationAdmin):
    pass
admin.site.register(FireDamage, FireDamageAdmin)

class FireWorkedAdmin(TranslationAdmin):
    pass
admin.site.register(FireWorked, FireWorkedAdmin)

class FiresAdmin(admin.ModelAdmin):
    pass
admin.site.register(Fires, FiresAdmin)

class Fires2ExtingCostsAdmin(admin.ModelAdmin):
    pass
admin.site.register(Fires2ExtingCosts, Fires2ExtingCostsAdmin)

class Fires2FireDamageAdmin(admin.ModelAdmin):
    pass
admin.site.register(Fires2FireDamage, Fires2FireDamageAdmin)

class Fires2FireWorkedAdmin(admin.ModelAdmin):
    pass
admin.site.register(Fires2FireWorked, Fires2FireWorkedAdmin)

class Fires2GeoPolygonAdmin(admin.ModelAdmin):
    pass
admin.site.register(Fires2GeoPolygon, Fires2GeoPolygonAdmin)


class MeteoconditionAdmin(admin.ModelAdmin):

    search_fields = ['=curdate']
    pass


class DecisionFinalAdmin(admin.ModelAdmin):

    list_display = ['id', '__unicode__', 'ground_square', 'crowning_square', 'unforest_square', 'wood_volume_per_ha', 'pjc', 'pr', 'sit_danger']

admin.site.register(DecisionFinal, DecisionFinalAdmin)



class DecisionWrongAdmin(admin.ModelAdmin):

    list_display = ['id', '__unicode__', 'ground_square', 'crowning_square', 'unforest_square', 'wood_volume_per_ha', 'pjc', 'pr', 'sit_danger']

admin.site.register(DecisionWrong, DecisionWrongAdmin)







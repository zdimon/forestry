# -*- coding: utf-8 -*-
from datetime import timedelta
from django.utils import timezone
from django.db import models
from django.utils.translation import ugettext as _
from django.db.models.signals import post_save, pre_save
from django.utils.timezone import datetime
from forestry import settings
from django.contrib.gis.db import models
from simple_translation.utils import get_language_from_request
from simple_translation.translation_pool import translation_pool
from paintstore.fields import ColorPickerField
from django.utils.html import mark_safe

# Create your models here.

class Post(models.Model):

    title = models.CharField(max_length=255,verbose_name=_(u'Название издательства')) # заголовок поста
    datetime = models.DateTimeField(u'Дата публикации') # дата публикации
    content = models.TextField(max_length=10000) # текст поста
    def __unicode__(self):
        return self.title

#Лесхозы#
class ForestryGroup(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
           	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        db_table = 'forestry_group'
        verbose_name=_(u'Лесхоз')
        verbose_name_plural=_(u'Лесхозы')


class ForestryGroupTranslation(models.Model):
    forestry_group = models.ForeignKey(ForestryGroup)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    name = models.CharField(max_length=250,default=False, verbose_name=_(u'Название'))
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'forestry_group_translation'

#Лесничества#
class Forestry(models.Model):
    forestry_group = models.ForeignKey('ForestryGroup')
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        db_table = 'forestry'
        verbose_name=_(u'Лесничество')
        verbose_name_plural=_(u'Лесничества')

class ForestryTranslation(models.Model):
    forestry = models.ForeignKey(Forestry)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES)
    name = models.CharField(max_length=250,default=False)
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'forestry_translation'

#Кварталы#
class GeoKvartal(models.Model):
    forestry = models.ForeignKey('forestry', verbose_name=_(u'Название лесхоза'))
    oid = models.IntegerField(default=0)
    number = models.IntegerField(verbose_name=_(u'Номер квартала'), default=0)
    area = models.DecimalField(verbose_name=_(u'Площадь'), max_digits=10, decimal_places=2, default=0)
    area_count = models.DecimalField(max_digits=10, decimal_places=2, default=0)
    perimetr = models.DecimalField(verbose_name=_(u'Периметр'), max_digits=10, decimal_places=2, default=0)
    center_zoom = models.IntegerField(default=0)
    center_lon = models.DecimalField(max_digits=10, decimal_places=7, default=0)
    center_lat = models.DecimalField(max_digits=10, decimal_places=7, default=0)
    geom = models.MultiPolygonField()
    def __unicode__(self):
        return str(self.number)

    class Meta:
        db_table = 'geo_kvartal'
        verbose_name=_(u'Квартал')
        verbose_name_plural=_(u'Кварталы')












#Типы выделов#
class TypePolygon(models.Model):
    rothermel = models.ForeignKey('Rothermel')
    is_pub = models.BooleanField(verbose_name=u'Активен', default=False)
    fill_color = ColorPickerField(verbose_name=u'Цвет заливки', blank=True, default= "#ff0000")
    border_color = ColorPickerField(verbose_name=u'Цвет границы', blank=True, default= "#000000")
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    @property
    def fill_color_rect(self):
        if(self.fill_color):
            return mark_safe(u'<div style="width: 25px; height: 25px; background:'+unicode(self.fill_color)+u' "></div>')
        else:
            return u'не установлен'

    @property
    def border_color_rect(self):
        if(self.fill_color):
            return mark_safe(u'<div style="width: 25px; height: 25px; background:'+unicode(self.border_color)+u' "></div>')
        else:
            return u'не установлен'

    class Meta:
        db_table = 'type_polygon'
        verbose_name=_(u'Тип выдела')
        verbose_name_plural=_(u'Типы выделов')
        #ordering = ['id']

class TypePolygonTranslation(models.Model):
    type_polygon = models.ForeignKey(TypePolygon)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    name = models.CharField(max_length=250,default=False, verbose_name=_(u'Тип выдела'))
    def __unicode__(self):
        return self.name
    class Meta:
        db_table = 'type_polygon_translation'

#Выделы#
class GeoPolygon(models.Model):
    type = models.ForeignKey('TypePolygon', verbose_name=_(u'Тип участка'))
    kvartal = models.ForeignKey('GeoKvartal', verbose_name=_(u'Номер квартала'))
    forestry = models.ForeignKey('forestry', verbose_name=_(u'Название лесничества'))
    oid = models.IntegerField(default=0)
    name = models.CharField(max_length=250, default=False)
    area = models.DecimalField(verbose_name=_(u'Площадь'), max_digits=20, decimal_places=2, default=0)
    perimetr = models.DecimalField(verbose_name=_(u'Периметр'), max_digits=20, decimal_places=2, default=0)
    full_date = models.BooleanField(default=False)
    is_geom = models.BooleanField(default=False)
    vydel = models.IntegerField(verbose_name=_(u'Номер выдела'), default=0)
    kvartal = models.IntegerField(verbose_name=_(u'Номер квартала'), default=0)
    center_zoom = models.IntegerField(default=0)
    center_lon = models.DecimalField(max_digits=25, decimal_places=7, default=0)
    center_lat = models.DecimalField(max_digits=25, decimal_places=7, default=0)
    fire_able = models.IntegerField(default=0)
    wood_volume_per_ha = models.DecimalField(verbose_name=_(u'Запас древесины на гектар'), max_digits=12, decimal_places=2, default=0)
    class_damage = models.IntegerField(verbose_name = (u'Класс ущерба'), default = 0)
    firedanger = models.DecimalField(verbose_name=_(u'Пожароопасность'), max_digits=21, decimal_places=5, default=0)
    influence_probabiliti = models.DecimalField(verbose_name=_(u'Вероятность влияния'), max_digits=12, decimal_places=2, default=0)
    firerisk = models.DecimalField(verbose_name=_(u'Лесопожарный риск'), max_digits=7, decimal_places=2, default=0)
    class_risk = models.IntegerField(verbose_name = (u'Класс лесопожарного риска'), default = 0)
    class_risk1 = models.IntegerField(verbose_name = (u'Класс лесопожарного риска'), default = 0)
    class_risk2 = models.IntegerField(verbose_name = (u'Класс лесопожарного риска'), default = 0)
    geom = models.MultiPolygonField('Country Border',srid=900913)
    objects = models.GeoManager()
    def __unicode__(self):
        return u'КВАРТАЛ ' + unicode(self.kvartal) + u', ВЫДЕЛ ' + unicode(self.vydel) + u'  (' + unicode(self.name) + u')'
    class Meta:
        verbose_name=_(u'Участок')
        verbose_name_plural=_(u'Участки')
        db_table = 'geo_polygon'

class ForestElement(models.Model):
    type_polygon = models.ForeignKey('TypePolygon', verbose_name=_(u'Тип выдела'))
    rothermel = models.ForeignKey('Rothermel')
    code = models.CharField(verbose_name=_(u'Шифр'), max_length=5,default=False)
    geo_polygons = models.ManyToManyField(GeoPolygon, through="ForestElement2GeoPolygon")
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        db_table = 'forest_element'
        verbose_name=_(u'Элемент леса')
        verbose_name_plural=_(u'Элементы леса')


class ForestElementTranslation(models.Model):
    forest_element = models.ForeignKey('ForestElement')
    name = models.CharField(verbose_name=_(u'Название'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'forest_element_translation'

class Rothermel(models.Model):
    veget_type_id = models.IntegerField(verbose_name=_(u'Vegetation type id'), default=0)
    reserve = models.DecimalField(verbose_name=_(u'The reserve of forest fuel, kg/m2'), max_digits=8, decimal_places=2, default=0)
    unit_area = models.DecimalField(verbose_name=_(u'Unit area, m-1'), max_digits=5, decimal_places=2, default=0)
    depth = models.DecimalField(verbose_name=_(u'Layer depth, m'), max_digits=5, decimal_places=2, default=0)
    h = models.DecimalField(verbose_name=_(u'Heating value of dry fuel, Joul/kg'), max_digits=8, decimal_places=2, default=0)
    ro = models.DecimalField(verbose_name=_(u'Density of dry fuel, kg/m3'), max_digits=8, decimal_places=2, default=0)
    mf = models.DecimalField(verbose_name=_(u'Moisture content'), max_digits=8, decimal_places=2, default=0)
    st = models.DecimalField(verbose_name=_(u'The mass part of minerals in forest fuel'), max_digits=8, decimal_places=2, default=0)
    se = models.DecimalField(verbose_name=_(u'Диаметр'), max_digits=8, decimal_places=2, default=0)
    u = models.DecimalField(verbose_name=_(u'Wind speed, m/sec'), max_digits=8, decimal_places=2, default=0)
    tg = models.DecimalField(verbose_name=_(u'The slope of the terrain (tangent)'), max_digits=8, decimal_places=2, default=0)
    mx = models.DecimalField(verbose_name=_(u'Critical moisture content'), max_digits=8, decimal_places=2, default=0)
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        db_table = 'Rothermel'
        verbose_name=_(u'Параметры модели Ротермела')
        verbose_name_plural=_(u'Параметры модели Ротермела')

class RothermelTranslation(models.Model):
    rothermel = models.ForeignKey('Rothermel')
    veget_type = models.CharField(verbose_name=_(u'Vegetation name'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'rothermel_translation'


class ForestElement2GeoPolygon(models.Model):
    geo_polygon = models.ForeignKey('GeoPolygon', verbose_name=_(u'Участок'), default=False)
    forest_element = models.ForeignKey('ForestElement', verbose_name=_(u'Элемент леса'), default=False)
    age = models.IntegerField(verbose_name=_(u'Возраст'), default=0)
    height = models.DecimalField(verbose_name=_(u'Высота'), max_digits=5, decimal_places=2, default=0)
    diameter = models.DecimalField(verbose_name=_(u'Диаметр'), max_digits=5, decimal_places=2, default=0)
    wood_store = models.DecimalField(verbose_name=_(u'Запас древесины'), max_digits=5, decimal_places=2, default=0)
    percent = models.IntegerField(verbose_name=_(u'Процент деловых деревьев'), default=0)
    def __unicode__(self):
        return unicode(self.geo_polygon) + u'  ' + unicode(self.forest_element) or u''
    class Meta:
        verbose_name=_(u'Элементы леса на выделе')
        verbose_name_plural=_(u'Элементы леса на выделе')
        db_table = 'forest_element2_geo_polygon'


#Виды параметров (select input)#
class TypeValue(models.Model):
    name = models.CharField(verbose_name=_(u'Тип значения (select, input)'), max_length=250)
    def __unicode__(self):
        return self.name
    class Meta:
        verbose_name=_(u'Тип значения')
        verbose_name_plural=_(u'Типы значений')
        db_table = 'type_value'


class TypeParamPolygon(models.Model):
    type_reg = models.ForeignKey('TypePolygon', verbose_name=_(u'Тип участка'), default=False)
    type_value = models.ForeignKey('TypeValue', verbose_name=_(u'Тип значения параметра'), null=True, default=False)
    value = models.CharField(verbose_name=_(u'Значение'), max_length=100, default=False)
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Тип параметра')
        verbose_name_plural=_(u'Типы параметров')
        db_table = 'type_param_polygon'

class TypeParamPolygonTranslation(models.Model):
    type_param_polygon = models.ForeignKey('TypeParamPolygon', verbose_name=_(u'Параметр'))
    name = models.CharField(verbose_name=_(u'Название параметра'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'type_param_polygon_translation'


class ParamValueSelect(models.Model):
    param = models.ForeignKey('TypeParamPolygon', default=False)
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Значение параметра для выбора')
        verbose_name_plural=_(u'Значения параметров для выбора')
        db_table = 'param_value_select'

class ParamValueSelectTranslation(models.Model):
    param_value_select = models.ForeignKey('ParamValueSelect')
    name = models.CharField(verbose_name=_(u'Название'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'type_param_polygon_translation'


class ValueParamPolygon(models.Model):
    type_reg = models.ForeignKey('TypePolygon',verbose_name=_(u'Тип участка'),  default=False)
    region = models.ForeignKey('GeoPolygon',verbose_name=_(u'Участок'),  default=False)
    type_param = models.ForeignKey('TypeParamPolygon',verbose_name=_(u'Параметр'),  default=False)
    value = models.CharField(verbose_name=_(u'Значение параметра'), max_length=100)
    def __unicode__(self):
        return unicode(self.region) + u'  ' + unicode(self.type_param)+ u'  -  ' +unicode(self.value) or u''
    class Meta:
        verbose_name=_(u'Значение параметра')
        verbose_name_plural=_(u'Значения параметров')
        db_table = 'value_param_polygon'

#СТАТИСТИКА ПОЖАРОВ#
class FireDetection(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Кем выявлен пожар')
        verbose_name_plural=_(u'Кем выявлен пожар')
        db_table = 'fire_detection'

class FireDetectionTranslation(models.Model):
    fire_detection = models.ForeignKey('FireDetection')
    name = models.CharField(verbose_name=_(u'Кем выявлен пожар'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'fire_detection_translation'

class FireCause(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Причина пожара')
        verbose_name_plural=_(u'Причины пожаров')
        db_table = 'fire_cause'

class FireCauseTranslation(models.Model):
    fire_cause = models.ForeignKey('FireCause')
    name = models.CharField(verbose_name=_(u'Причина пожара'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'fire_cause_translation'


class ExtingCosts(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Виды затрат на тушение')
        verbose_name_plural=_(u'Виды затрат на тушение')
        db_table = 'exting_costs'

class ExtingCostsTranslation(models.Model):
    exting_costs = models.ForeignKey('ExtingCosts')
    name = models.CharField(verbose_name=_(u'Вид затрат на тушение'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'exting_costs_translation'


class FireDamage(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Ущерб от пожара')
        verbose_name_plural=_(u'Ущерб от пожара')
        db_table = 'fire_damage'

class FireDamageTranslation(models.Model):
    fire_damage = models.ForeignKey('FireDamage')
    name = models.CharField(verbose_name=_(u'Ущерб от пожара'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'fire_damage_translation'

class FireWorked(models.Model):
    def __unicode__(self):
        return getattr(translation_pool.annotate_with_translations(self), 'translations', []) \
            	and unicode(translation_pool.annotate_with_translations(self).translations[0]) or u'No translations'
    class Meta:
        verbose_name=_(u'Отработано на пожаре')
        verbose_name_plural=_(u'Отработано на пожаре')
        db_table = 'fire_worked'

class FireWorkedTranslation(models.Model):
    fire_worked = models.ForeignKey('FireWorked')
    name = models.CharField(verbose_name=_(u'Отработано на пожаре'), max_length=250,default=False)
    lang = models.CharField(max_length=2, choices=settings.LANGUAGES, verbose_name=_(u'Язык'))
    def __unicode__(self):
        return unicode(self.name) or u''
    class Meta:
        db_table = 'fire_worked_translation'

class Fires(models.Model):
    fire_detection = models.ForeignKey('FireDetection', verbose_name=_(u'Кем выявлен'), default=False)
    forestry_id = models.ForeignKey('Forestry', verbose_name=_(u'Лесничество'), null=True, db_column='forestry_id', default=False)
    fire_cause = models.ForeignKey('FireCause', verbose_name=_(u'Причина пожара'), default=False)
    date_begin = models.DateTimeField(verbose_name=_(u'Дата-время начала пожара'), default=False)
    date_end = models.DateTimeField(verbose_name=_(u'Дата-время окончания пожара'), default=False)
    exting_begin = models.DateTimeField(verbose_name=_(u'Дата-время тушения'), default=False)
    exting_end = models.DateTimeField(verbose_name=_(u'Дата-время окончания тушения'), default=False)
    square = models.DecimalField(verbose_name=_(u'Общая выгоревшая площадь'), max_digits=5, decimal_places=2, default=0)
    crowning_square = models.DecimalField(verbose_name=_(u'Площадь верхового пожара'), max_digits=5, decimal_places=2, default=0)
    ground_square = models.DecimalField(verbose_name=_(u'Площадь низового пожара'), max_digits=5, decimal_places=2, default=0)
    unforest_square = models.DecimalField(verbose_name=_(u'Выгоревшая нелесная площадь'), max_digits=5, decimal_places=2, default=0)
    date_month = models.IntegerField(verbose_name=_(u'День месяца'), default=0)
    def __unicode__(self):
        return unicode(self.date_begin) + u'  ' + unicode(self.forestry_id) or u''
    class Meta:
        verbose_name=_(u'Пожар')
        verbose_name_plural=_(u'Пожары')
        db_table = 'fires'
        ordering = ['date_begin']

class Fires2ExtingCosts(models.Model):
    fire = models.ForeignKey('Fires', verbose_name=_(u'Пожар'), default=False)
    exting_costs = models.ForeignKey('ExtingCosts', verbose_name=_(u'Затраты на тушение'), default=False)
    sum = models.DecimalField(verbose_name=_(u'Сумма затрат'), max_digits=10, decimal_places=2, default=0)
    def __unicode__(self):
        return unicode(self.fire) + u':  ' + unicode(self.exting_costs) + u' - ' + unicode(self.sum) or u''
    class Meta:
        verbose_name=_(u'Затраты на тушение для каждого пожара')
        verbose_name_plural=_(u'Затраты на тушения для каждого пожара')
        db_table = 'fires2_exting_costs'

class Fires2FireDamage(models.Model):
    fire = models.ForeignKey('Fires', verbose_name=_(u'Пожар'), default=False)
    fire_damage = models.ForeignKey('FireDamage', verbose_name=_(u'Ущерб от пожара'), default=False)
    sum = models.DecimalField(verbose_name=_(u'Сумма ущерба'), max_digits=10, decimal_places=2, default=0)
    def __unicode__(self):
        return unicode(self.fire) + u':  ' + unicode(self.fire_damage) + u' - ' + unicode(self.sum) or u''
    class Meta:
        verbose_name=_(u'Ущерб от каждого пожара')
        verbose_name_plural=_(u'Ущерб от каждого пожара')
        db_table = 'fires2_fire_damage'

class Fires2FireWorked(models.Model):
    fire = models.ForeignKey('Fires', verbose_name=_(u'Пожар'), default=False)
    fire_worked = models.ForeignKey('FireWorked', verbose_name=_(u'Отработано на пожаре'), default=False)
    num = models.DecimalField(verbose_name=_(u'Количество'), max_digits=5, decimal_places=2, default=0)
    def __unicode__(self):
        return unicode(self.fire) + u':  ' + unicode(self.fire_worked) + u' - ' + unicode(self.num) or u''
    class Meta:
        verbose_name=_(u'Число отработанных на пожаре человекодней или машиносмен')
        verbose_name_plural=_(u'Число отработанных на пожаре человекодней или машиносмен')
        db_table = 'fires2_fire_worked'

class Fires2GeoPolygon(models.Model):
    fire = models.ForeignKey('Fires', default=False)
    geo_polygon = models.ForeignKey('GeoPolygon', default=False)
    kvartal = models.IntegerField(verbose_name=_(u'Номер квартала'), default=0)
    vydel = models.IntegerField(verbose_name=_(u'Номер выдела'), default=0)
    def __unicode__(self):
     #   return unicode(self.fire) + u':  ' + unicode(self.geo_polygon) + u' - ' + unicode(self.kvartal) or u''
        return unicode(self.fire) or u''
    class Meta:
        verbose_name=_(u'Пожар, привязанный к местности')
        verbose_name_plural=_(u'Пожары, привязанные к местности')
        db_table = 'fires2_geo_polygon'


class Meteocondition(models.Model):
    curdate = models.DateField(verbose_name=_(u'Дата'))
    t = models.DecimalField(verbose_name=_(u'Температура'), max_digits=5, decimal_places=2, default=0)
    p0 = models.DecimalField(verbose_name=_(u'атмосферное давление на уровне станции (мм ртутного столбца)'), max_digits=5, decimal_places=2, default=0, blank = True)
    p = models.DecimalField(verbose_name=_(u'атмосферное давление, приведенное к среднему уровню моря (мм ртутного столбца)'), max_digits=5, decimal_places=2, blank = True)
    pa = models.DecimalField(verbose_name=_(u'барическая тенденция: изменение атмосферного давления за последние три часа (мм ртутного столбца)'), max_digits=5, decimal_places=2,blank = True)
    u = models.IntegerField(verbose_name=_(u'относительная влажность (%) на высоте 2 метра над поверхностью земли'), blank = True)
    dd = models.CharField(verbose_name=_(u'направление ветра (румбы) на высоте 10-12 метров над землей, осредненное за 10-минутный период, непосредственное предшествовавший сроку наблюдения'), max_length=50, blank = True)
    ff = models.IntegerField(verbose_name=_(u'скорость ветра на высоте 10-12 метров над землей, осредненное за 10-минутный период, непосредственное предшествовавший сроку наблюдения (метры в секунду)'), blank = True)
    ff10 = models.IntegerField(verbose_name=_(u'максимальное значение порыва ветра на высоте 10-12 метров над землей, осредненное за 10-минутный период, непосредственное предшествовавший сроку наблюдения (метры в секунду)'), blank = True)
    ff3 = models.IntegerField(verbose_name=_(u'максимальное значение порыва ветра на высоте 10-12 метров над землей за период между сроками (метры в секунду)'),blank = True, null = True)
    n = models.CharField(verbose_name=_(u'общая облачность (%)'), max_length=50, blank = True)
    ww = models.CharField(verbose_name=_(u'текущая погода, сообщаемая с метеостанции'), max_length=100, blank = True)
    w1 = models.CharField(verbose_name=_(u'прошедшая погода между сроками наблюдения 1'), max_length=100, blank = True)
    w2 = models.CharField(verbose_name=_(u'прошедшая погода между сроками наблюдения 2'), max_length=100, blank = True)
    tn = models.DecimalField(verbose_name=_(u'минимальная температура воздуха (градусы Цельсия) за прошедший период (не более 12 часов))'), max_digits=5, decimal_places=2, blank = True)
    tx = models.DecimalField(verbose_name=_(u'максимальная температура воздуха (градусы Цельсия) за прошедший период (не более 12 часов))'), max_digits=5, decimal_places=2,blank = True)
    cl = models.CharField(verbose_name=_(u'слоисто-кучевые, слоистые, кучевые и кучево-дождевые облака'), max_length=100, blank = True)
    nh = models.CharField(verbose_name=_(u'количество всех наблюдавшихся облаков cl или, при отсутствии облаков cl, количество всех наблюдавшихся облаков cm (%)'), max_length=30, blank = True)
    h = models.CharField(verbose_name=_(u'высота основания самых низких облаков (м)'), max_length=20, blank = True)
    cm = models.CharField(verbose_name=_(u'высококучевые, высокослоистые и слоисто-дождевые облака'), max_length=100, blank = True)
    ch = models.CharField(verbose_name=_(u'перистые, перисто-кучевые и перисто-слоистые облака'), max_length=100, blank = True)
    w = models.DecimalField(verbose_name=_(u'горизонтальная дальность видимости (км)'), max_digits=5, decimal_places=2, blank = True)
  
    r = models.DecimalField(verbose_name=_(u'температура точки росы на высоте 2 м над поверхностью земли (градусы Цельсия)'), max_digits=5, decimal_places=2, default=0)
    rrr = models.DecimalField(verbose_name=_(u'количество выпавших осадков (миллиметры)'), max_digits=5, decimal_places=2, default=0)
    tr = models.IntegerField(verbose_name=_(u'период времени, за который накоплено указанное количество осадков'), blank = True)
    e = models.CharField(verbose_name=_(u'состояние поверхности почвы без снега или измеримого ледяного покрова'), max_length=100, blank = True)
    tg = models.DecimalField(verbose_name=_(u'минимальная температура поверхности почвы за ночь (градусы Цельсия)'), max_digits=5, decimal_places=2, blank = True)
    es = models.CharField(verbose_name=_(u'состояние поверхности почвы со снегом или измеримым ледяным покровом'), max_length=100, blank = True)
    sss = models.IntegerField(verbose_name=_(u'высота снежного покрова (см)'), blank = True)
    rain = models.BooleanField(verbose_name=_(u'наличие дождя'), blank = True)
    # Рассчетные показатели
    kmp = models.DecimalField(verbose_name=_(u'комплексный метеорологический показатель'), max_digits=20, decimal_places=10, blank = True)
    pjc = models.DecimalField(verbose_name=_(u'вероятность по метеоусловиям'), max_digits=20, decimal_places=10, blank = True)
    ap = models.DecimalField(verbose_name=_(u'вероятность по антропогенной причине'), max_digits=20, decimal_places=10, blank = True)
    pm = models.DecimalField(verbose_name=_(u'вероятность по причине грозовой активности'), max_digits=20, decimal_places=10, blank = True)
    pr = models.DecimalField(verbose_name=_(u'общая вероятность возникновения пожара'), max_digits=20, decimal_places=10, blank = True)
    def __unicode__(self):
     #   return unicode(self.fire) + u':  ' + unicode(self.geo_polygon) + u' - ' + unicode(self.kvartal) or u''
        return unicode(self.curdate) or u''
    class Meta:
        verbose_name=_(u'Метеоусловия')
        verbose_name_plural=_(u'Метеоусловия')
        db_table = 'meteocondition'


class FiresNumber(models.Model):
    date_month = models.IntegerField(verbose_name=_(u'день месяца'), default=0)
    number = models.IntegerField(verbose_name=_(u'число пожаров'), default=0)

class FireLocation(models.Model):
    fire = models.ForeignKey('Fires', default=False)
    geo_polygon = models.ForeignKey('GeoPolygon', default=False)
    kvartal = models.IntegerField(verbose_name=_(u'Номер квартала'), default=0)
    vydel = models.IntegerField(verbose_name=_(u'Номер выдела'), default=0)


class DecisionTable(models.Model):

#CONDITION ATTRIBUTES

    fire = models.ForeignKey('Fires', related_name = "fire")
    crowning_square = models.DecimalField(verbose_name=_(u'Crown fire area'), max_digits=12, decimal_places=2, null=True)
    ground_square = models.DecimalField(verbose_name=_(u'Ground fire area'), max_digits=12, decimal_places=2, blank = True, null=True)
    unforest_square = models.DecimalField(verbose_name=_(u'Non-forest area'), max_digits=12, decimal_places=2, blank = True, null=True)
    type_polygon = models.IntegerField(verbose_name=_(u'Region type'), default=1)
    wood_volume_per_ha = models.DecimalField(verbose_name=_(u'Wood volume per ha'), max_digits=12, decimal_places=2, default=0)
  
#Mateocondition table
    pjc = models.DecimalField(verbose_name=_(u'Nesterov index'), max_digits=5, decimal_places=2, default=0)
    pr = models.DecimalField(verbose_name=_(u'Total fire danger'), max_digits=5, decimal_places=2, default=0)

#DECISION ATTRIBUTES
    forestry_man_days = models.DecimalField(verbose_name=_(u'Forestry, man-daus'), max_digits=8, decimal_places=4, default=0)
    emergency_man_days = models.DecimalField(verbose_name=_(u'Emergency, man-days'), max_digits=8, decimal_places=4, default=0)
    another_man_days = models.DecimalField(verbose_name=_(u'Another services, man-days'), max_digits=8, decimal_places=4, default=0)
    forestry_fire_eng = models.DecimalField(verbose_name=_(u'Forestry,fire engines, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    forestry_another = models.DecimalField(verbose_name=_(u'Forestry, another equipment, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    emergency_fire_eng = models.DecimalField(verbose_name=_(u'Emergency, fire engines, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    emergency_another = models.DecimalField(verbose_name=_(u'Emergency, another equipment, machine-shifts'), max_digits=8, decimal_places=4, default=0)    
    another_fire_eng = models.DecimalField(verbose_name=_(u'Another services, fire engines, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    another_another = models.DecimalField(verbose_name=_(u'Another services,another equipment, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    total_fire_eng = models.DecimalField(verbose_name=_(u'In total, fire engines, machine-shifts'), max_digits=8, decimal_places=4, default=0)
    total_another = models.DecimalField(verbose_name=_(u'In total, another equipment, machine-shifts'), max_digits=8, decimal_places=4, default=0)
#Assesment of the situation
    assessment = models.IntegerField(verbose_name=_(u'Assessment'), default=1)












class DecisionFinal(models.Model):

#CONDITION ATTRIBUTES 
    fire = models.ForeignKey('Fires', related_name = "fire1")
    type_polygon = models.IntegerField(verbose_name=_(u'Region type'), default=1)
    ground_square = models.CharField(max_length=20, verbose_name=_(u'Ground fire area'), default='zero')
    crowning_square = models.CharField(max_length=20, verbose_name=_(u'Crown fure area'), default='zero')
    unforest_square = models.CharField(max_length=20, verbose_name=_(u'Non-forest area'), default='zero')
    wood_volume_per_ha = models.CharField(max_length=10, verbose_name=_(u'Wood volume per ha'), default='zero')
#Mateocondition table
    pjc = models.CharField(max_length=20, verbose_name=_(u'Nesterov index'), default='zero')
    pr = models.CharField(max_length=20, verbose_name=_(u'Total fire danger'), default='zero')

#DECISION ATTRIBUTES
    forestry_man_days = models.CharField(max_length=20, verbose_name=_(u'Forestry, man-days'), default='zero')
    emergency_man_days = models.CharField(max_length=20, verbose_name=_(u'Emergency, man-days'), default='zero')
    another_man_days = models.CharField(max_length=20, verbose_name=_(u'Another services, man-days'), default='zero')
    forestry_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Forestry, fire engines, machine-shifts'), default='zero')
    
    forestry_another = models.CharField(max_length=20, verbose_name=_(u'Forestry, another equipment, machine-shifts'), default='zero')
    
    emergency_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Emergency, fire engines, machine-shifts'), default='zero')
    
    emergency_another = models.CharField(max_length=20, verbose_name=_(u'Emergency, another equipment, mashine-shifts'), default='zero')
    
    another_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Another services, fire engines, machine-shifts'), default='zero')
    another_another = models.CharField(max_length=20, verbose_name=_(u'Another services, another equipment, machine-shift'), default='zero')
#Assesment of the situation
    sit_danger = models.CharField(max_length=20, verbose_name=_(u'Assessment'), default='zero')

    def __unicode__(self):
     #   return unicode(self.fire) + u':  ' + unicode(self.geo_polygon) + u' - ' + unicode(self.kvartal) or u''
        return unicode(self.fire) or u''
    class Meta:
        verbose_name=_(u'Fire')
        verbose_name_plural=_(u'Information table')
        ordering = ['ground_square', 'crowning_square', 'unforest_square', 'wood_volume_per_ha', 'pjc', 'pr', 'sit_danger']





#Table which contains rows with the same condition attribute values and different decision attribute values
class DecisionWrong(models.Model):

#CONDITION ATTRIBUTES 
    fire = models.ForeignKey('Fires', related_name = "fire2")
    type_polygon = models.IntegerField(verbose_name=_(u'Region type'), default=1)
    ground_square = models.CharField(max_length=20, verbose_name=_(u'Ground fire area'), default='zero')
    crowning_square = models.CharField(max_length=20, verbose_name=_(u'Crown fure area'), default='zero')
    unforest_square = models.CharField(max_length=20, verbose_name=_(u'Non-forest area'), default='zero')
    wood_volume_per_ha = models.CharField(max_length=10, verbose_name=_(u'Wood volume per ha'), default='zero')
#Mateocondition table
    pjc = models.CharField(max_length=20, verbose_name=_(u'Nesterov index'), default='zero')
    pr = models.CharField(max_length=20, verbose_name=_(u'Total fire danger'), default='zero')

#DECISION ATTRIBUTES
    forestry_man_days = models.CharField(max_length=20, verbose_name=_(u'Forestry, man-days'), default='zero')
    emergency_man_days = models.CharField(max_length=20, verbose_name=_(u'Emergency, man-days'), default='zero')
    another_man_days = models.CharField(max_length=20, verbose_name=_(u'Another services, man-days'), default='zero')
    forestry_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Forestry, fire engines, machine-shifts'), default='zero')
    
    forestry_another = models.CharField(max_length=20, verbose_name=_(u'Forestry, another equipment, machine-shifts'), default='zero')
    
    emergency_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Emergency, fire engines, machine-shifts'), default='zero')
    
    emergency_another = models.CharField(max_length=20, verbose_name=_(u'Emergency, another equipment, mashine-shifts'), default='zero')
    
    another_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Another services, fire engines, machine-shifts'), default='zero')
    another_another = models.CharField(max_length=20, verbose_name=_(u'Another services, another equipment, machine-shift'), default='zero')
#Assesment of the situation
    sit_danger = models.CharField(max_length=20, verbose_name=_(u'Assessment'), default='zero')
    def __unicode__(self):
        return unicode(self.fire) or u''
    class Meta:
        verbose_name=_(u'Fire')
        verbose_name_plural=_(u'Wrong rows in Decision table')
        ordering = ['ground_square', 'crowning_square', 'unforest_square', 'wood_volume_per_ha', 'pjc', 'pr', 'sit_danger']




#Table which contains rows without inconclusive information
class DecisionReduct(models.Model):

#CONDITION ATTRIBUTES 
    fire = models.ForeignKey('Fires', related_name = "fire3")
    type_polygon = models.IntegerField(verbose_name=_(u'Region type'), default=1)
    ground_square = models.CharField(max_length=20, verbose_name=_(u'Ground fire area'), default='zero')
    crowning_square = models.CharField(max_length=20, verbose_name=_(u'Crown fure area'), default='zero')
    unforest_square = models.CharField(max_length=20, verbose_name=_(u'Non-forest area'), default='zero')
    wood_volume_per_ha = models.CharField(max_length=10, verbose_name=_(u'Wood volume per ha'), default='zero')
#Mateocondition table
    pjc = models.CharField(max_length=20, verbose_name=_(u'Nesterov index'), default='zero')
    pr = models.CharField(max_length=20, verbose_name=_(u'Total fire danger'), default='zero')

#DECISION ATTRIBUTES
    forestry_man_days = models.CharField(max_length=20, verbose_name=_(u'Forestry, man-days'), default='zero')
    emergency_man_days = models.CharField(max_length=20, verbose_name=_(u'Emergency, man-days'), default='zero')
    another_man_days = models.CharField(max_length=20, verbose_name=_(u'Another services, man-days'), default='zero')
    forestry_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Forestry, fire engines, machine-shifts'), default='zero')
    
    forestry_another = models.CharField(max_length=20, verbose_name=_(u'Forestry, another equipment, machine-shifts'), default='zero')
    
    emergency_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Emergency, fire engines, machine-shifts'), default='zero')
    
    emergency_another = models.CharField(max_length=20, verbose_name=_(u'Emergency, another equipment, mashine-shifts'), default='zero')
    
    another_fire_eng = models.CharField(max_length=20, verbose_name=_(u'Another services, fire engines, machine-shifts'), default='zero')
    another_another = models.CharField(max_length=20, verbose_name=_(u'Another services, another equipment, machine-shift'), default='zero')
#Assesment of the situation
    sit_danger = models.CharField(max_length=20, verbose_name=_(u'Assessment'), default='zero')
    def __unicode__(self):
        return unicode(self.fire) or u''
    class Meta:
        verbose_name=_(u'Reduct')
        verbose_name_plural=_(u'Reduct')
        ordering = ['ground_square', 'crowning_square', 'unforest_square', 'wood_volume_per_ha', 'pjc', 'pr', 'sit_danger']












class Attributes(models.Model):
    name = models.CharField(max_length=50, default=False)
    bound1 = models.DecimalField(verbose_name=_(u''), max_digits=8, decimal_places=4, default=0)
    bound2 = models.DecimalField(verbose_name=_(u''), max_digits=8, decimal_places=4, default=0)
    bound3 = models.DecimalField(verbose_name=_(u''), max_digits=8, decimal_places=4, default=0)
    bound4 = models.DecimalField(verbose_name=_(u''), max_digits=8, decimal_places=4, default=0)
    bound5 = models.DecimalField(verbose_name=_(u''), max_digits=8, decimal_places=4, default=0)


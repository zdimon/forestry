from django.conf.urls import patterns, url

from map import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^kvartal-info$', views.kvartalinfo, name='get-kvartal-info'),
     url(r'^gegion-info$', views.regioninfo, name='get-region-info'),
)
from django.conf import settings
from django.shortcuts import render_to_response
from djgeojson.views import GeoJSONLayerView

def welcome(request):
    base_url = request.build_absolute_uri('/')[:-1]
    return render_to_response('home.html', {"base_url": base_url})

def mapforest(request):
    return render_to_response('forest.html', {})


class MapLayer(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization

class ForestLayer(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
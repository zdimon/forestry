from models import ForestryGroup, ForestryGroupTranslation
from models import Forestry, ForestryTranslation
from models import TypePolygon, TypePolygonTranslation
from models import ForestElement, ForestElementTranslation
from models import TypeParamPolygon, TypeParamPolygonTranslation
from models import FireDetection, FireDetectionTranslation
from models import FireCause, FireCauseTranslation
from models import ExtingCosts, ExtingCostsTranslation
from models import FireDamage, FireDamageTranslation
from models import FireWorked, FireWorkedTranslation

from simple_translation.translation_pool import translation_pool

translation_pool.register_translation(ForestryGroup, ForestryGroupTranslation, language_field='lang')

translation_pool.register_translation(Forestry, ForestryTranslation, language_field='lang')

translation_pool.register_translation(TypePolygon, TypePolygonTranslation, language_field='lang')

translation_pool.register_translation(ForestElement, ForestElementTranslation, language_field='lang')

translation_pool.register_translation(TypeParamPolygon, TypeParamPolygonTranslation, language_field='lang')

translation_pool.register_translation(FireDetection, FireDetectionTranslation, language_field='lang')

translation_pool.register_translation(FireCause, FireCauseTranslation, language_field='lang')

translation_pool.register_translation(ExtingCosts, ExtingCostsTranslation, language_field='lang')

translation_pool.register_translation(FireDamage, FireDamageTranslation, language_field='lang')

translation_pool.register_translation(FireWorked, FireWorkedTranslation, language_field='lang')


from django.core.management.base import BaseCommand, CommandError
from map.models import Fires, GeoPolygon, DecisionTable, Meteocondition, Fires2FireWorked, FireWorked, Fires2GeoPolygon, DecisionFinal, Attributes
from random import randint
from django.db.models import Avg, Max, Min
from decimal import Decimal
from decimal import *



class Command(BaseCommand):

    def handle(self, *args, **options):

        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id

            df = DecisionFinal()

            df.fire_id = f
 
           
            w =  dt.wood_volume_per_ha
            a = Attributes.objects.filter(name = "wood_volume_per_ha")
            if a[0].bound1 <= w < a[0].bound2:
                df.wood_volume_per_ha = 'small'
 #           import pdb;pdb.set_trace()
               
            elif a[0].bound2 <= w < a[0].bound3:
                df.wood_volume_per_ha = 'average'

            elif a[0].bound3 <= dt.wood_volume_per_ha < a[0].bound4:
                df.wood_volume_per_ha = 'big'
            else:
                df.wood_volume_per_ha = 'very big'
            print f
            print a[0].bound1
            print a[0].bound2
            print dt.wood_volume_per_ha
            print df.wood_volume_per_ha
            print ""

            df.save()    
                




        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 

            df = DecisionFinal.objects.get(fire_id = f)
           
            w =  dt.crowning_square
            a = Attributes.objects.filter(name = "crowning_square")
            if a[0].bound1 < w < a[0].bound2:
                df.crowning_square = 'small' 
            elif w == None:
                df.crowning_square = 'zero'
            elif w == 0:
                df.crowning_square = 'zero'
                
  #          import pdb;pdb.set_trace()
                
            elif a[0].bound2 <= w < a[0].bound3:
                df.crowning_square = 'average'

            elif a[0].bound3 <= w < a[0].bound4:
                df.crowning_square = 'big'
            else:
                df.crowning_square = 'very big'
#            print f
#            print a[0].bound1
#            print a[0].bound2
#            print w
#            print df.crowning_square
#            print ""

            df.save()    




        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 

            df = DecisionFinal.objects.get(fire_id = f)
           
            w =  dt.ground_square
            a = Attributes.objects.filter(name = "ground_square")
            if a[0].bound1 < w < a[0].bound2:
                df.ground_square = 'small'
            elif w == None:
                df.ground_square = 'zero'
            elif w == 0:
                df.ground_square = 'zero'
                
  #          import pdb;pdb.set_trace()
                
            elif a[0].bound2 <= w < a[0].bound3:
                df.ground_square = 'average'

            elif a[0].bound3 <= w < a[0].bound4:
                df.ground_square = 'big'
            else:
                df.ground_square = 'very big'
#            print f
#            print w
#            print df.ground_square
#            print ""

            df.save()    




        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 

            df = DecisionFinal.objects.get(fire_id = f)
           
            w =  dt.unforest_square
            a = Attributes.objects.filter(name = "unforest_square")
            if a[0].bound1 < w < a[0].bound2:
                df.unforest_square = 'small'
            elif w == None:
                df.unforest_square = 'zero'
            elif w == 0:
                df.unforest_square = 'zero'
                
  #          import pdb;pdb.set_trace()
                
            elif a[0].bound2 <= w < a[0].bound3:
                df.unforest_square = 'average'

            elif a[0].bound3 <= w < a[0].bound4:
                df.unforest_square = 'big'
            else:
                df.unforest_square = 'very big'
#            print f
#            print w
#            print df.unforest_square
#            print ""

            df.save()    




        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 

            df = DecisionFinal.objects.get(fire_id = f)
           
            w =  dt.pjc
            a = Attributes.objects.filter(name = "pjc")
            if a[0].bound1 < w < a[0].bound2:
                df.pjc = 'small'
            elif w == None:
                df.pjc = 'zero'
            elif w == 0:
                df.pjc = 'zero'
                
  #          import pdb;pdb.set_trace()
                
            elif a[0].bound2 <= w < a[0].bound3:
                df.pjc = 'average'

            elif a[0].bound3 <= w < a[0].bound4:
                df.pjc = 'big'
            else:
                df.unforest_square = 'very big'
#            print f
#            print w
#            print df.pjc
#            print ""

            df.save()    





        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 

            df = DecisionFinal.objects.get(fire_id = f)
           
            w =  dt.pr
            a = Attributes.objects.filter(name = "pr")
            if a[0].bound1 < w < a[0].bound2:
                df.pr = 'small'
            elif w == None:
                df.pr = 'zero'
            elif w == 0:
                df.pr = 'zero'
                
  #          import pdb;pdb.set_trace()
                
            elif a[0].bound2 <= w < a[0].bound3:
                df.pr = 'average'

            elif a[0].bound3 <= w < a[0].bound4:
                df.pr = 'big'
            else:
                df.pr = 'very big'
 #           print f
  #          print w
#            print df.pr
#            print ""

            df.save()    


#Decision-attributes

        for dt in  DecisionTable.objects.all():
            f =  dt.fire_id
 
            df = DecisionFinal.objects.get(fire_id = f)
          
            if dt.forestry_man_days != 0:
                df.forestry_man_days = '1'
            if dt.forestry_man_days == 0:
                df.forestry_man_days = '0'


            if dt.emergency_man_days != 0:
                df.emergency_man_days = '1'
            if dt.emergency_man_days == 0:
                df.emergency_man_days = '0'


            if dt.another_man_days != 0:
                df.another_man_days = '1'
            if dt.another_man_days == 0:
                df.another_man_days = '0'


            if dt.forestry_fire_eng != 0:
                df.forestry_fire_eng = '1'
            if dt.forestry_fire_eng == 0:
                df.forestry_fire_eng = '0'


            if dt.forestry_another != 0:
                df.forestry_another = '1'
            if dt.forestry_another == 0:
                df.forestry_another = '0'

            if dt.emergency_fire_eng != 0:
                df.emergency_fire_eng = '1'
            if dt.emergency_fire_eng == 0:
                df.emergency_fire_eng = '0'

            if dt.emergency_another != 0:
                df.emergency_another = '1'
            if dt.emergency_another == 0:
                df.emergency_another = '0'

            if dt.another_fire_eng != 0:
                df.another_fire_eng = '1'
            if dt.another_fire_eng == 0:
                df.another_fire_eng = '0'

            if dt.another_another != 0:
                df.another_another = '1'
            if dt.another_another == 0:
                df.another_another = '0'


            df.save()    




        for df in  DecisionFinal.objects.all():
            if df.forestry_man_days=='0' and df.emergency_man_days=='0' and df.another_man_days=='0' and df.forestry_fire_eng=='0' and df.forestry_another=='0':
              #  print 'hahaha'
                df.sit_danger = 'zero'
            if df.forestry_man_days=='1' and df.emergency_man_days=='0' and df.another_man_days=='0' and df.forestry_fire_eng=='1' and df.forestry_another=='0':
         #       print 'hahaha'
                df.sit_danger = 'low'
            if df.forestry_man_days=='1' and df.emergency_man_days=='0' and df.another_man_days=='0' and df.forestry_fire_eng=='1' and df.forestry_another=='1':
                df.sit_danger = 'average'
            if df.forestry_man_days=='1' and df.emergency_man_days=='1' and df.another_man_days=='0' and df.forestry_fire_eng=='1' and df.another_fire_eng=='0':
                df.sit_danger = 'high'
            if df.forestry_man_days=='1' and df.emergency_man_days=='1' and df.another_man_days=='1':
           #     print 'haha'
                df.sit_danger = 'critical'
            df.save()    








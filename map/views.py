#coding: UTF-8

from django.conf import settings
from django.shortcuts import render_to_response
from djgeojson.views import GeoJSONLayerView

from map.models import GeoPolygon, Fires, Meteocondition, ForestElement2GeoPolygon
from qsstats import QuerySetStats
from django.db.models import Count
from django.db.models import Max
from django.db.models import Sum
from django.db.models import Variance
from datetime import date

from datetime import datetime

from highcharts.views import HighChartsLineView




from djgeojson.serializers import Serializer as GeoJSONSerializer
from djgeojson.http import HttpJSONResponse


def index(request):
    return HttpResponse("Hello, world. You're at the poll index.")

def kvartalinfo(request):
    from map.models import GeoKvartal
    kvartal_id = request.GET['id']
    kvartal = GeoKvartal.objects.all().get(pk=kvartal_id)
    base_url = request.build_absolute_uri('/')[:-1]
#    fe = ForestElement2GeoPolygon.objects.all().filter(geo_polygon=region_id)
    return render_to_response('kvartal_info.html', {"kvartal_id": kvartal_id, "kvartal": kvartal, "base_url": base_url })
#    return render_to_response('kvartal_info.html', {"kvartal_id": kvartal_id})

def regioninfo(request):
    from map.models import GeoPolygon
    region_id = request.GET['id']
    region = GeoPolygon.objects.all().get(pk=region_id)
    base_url = request.build_absolute_uri('/')[:-1]
    fe = ForestElement2GeoPolygon.objects.all().filter(geo_polygon=region_id)
    return render_to_response('region_info.html', {"region_id": region_id, "region": region, "fe": fe, "base_url": base_url })



def mapregion(request):
    from map.models import TypePolygon
    types = TypePolygon.objects.all().filter(is_pub=True).order_by('fill_color')
    base_url = request.build_absolute_uri('/')[:-1]
    return render_to_response('region.html', {"types":types, "base_url": base_url})



def firedanger(request):

    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['1', '#ffffff'],
              ['2', '#FFC1C1'],
              ['3', '#FFAEB9'],
              ['4', '#FF82AB'],
              ['5', '#FA8072'],
              ['6', '#FF6A6A'],
              ['7', '#FF4040'],
              ['8', '#FF3030'],
              ['9', '#FF0000']]
    return render_to_response('firedanger.html', {"base_url": base_url,"legend": legend})





def firedamage(request):

    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['0', '#ffffff'],
              ['0-66', '#FFAEB9'],
              ['66-132', '#FA8072'],
              ['132 - 198', '#FF4040'],
              ['> 198', '#FF0000']]
    return render_to_response('firedamage.html', {"base_url": base_url,"legend": legend})





def firerisk(request):

    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['0', '#90EE90'],
              ['0 - 9', '#FFAEB9'],
              ['9-18', '#FA8072'],
              ['18-27', '#FF4040'],
              ['> 27', '#FF0000']]
    return render_to_response('firerisk.html', {"base_url": base_url,"legend": legend})




def firerisk1(request):

    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['0', '#90EE90'],
              ['0 - 9', '#FFAEB9'],
              ['9-18', '#FA8072'],
              ['18-27', '#FF4040'],
              ['> 27', '#FF0000']]
    return render_to_response('firerisk1.html', {"base_url": base_url,"legend": legend})



def firerisk2(request):

    base_url = request.build_absolute_uri('/')[:-1]
    legend = [['0', '#90EE90'],
              ['0 - 9', '#FFAEB9'],
              ['9-18', '#FA8072'],
              ['18-27', '#FF4040'],
              ['> 27', '#FF0000']]
    return render_to_response('firerisk2.html', {"base_url": base_url,"legend": legend})





def graph(request):

#    base_url = request.build_absolute_uri('/')[:-1]
 #   legend = [['0', '#ffffff'],
  #            ['0 - 9', '#FFAEB9'],
   #           ['9-18', '#FA8072'],
    #          ['18-27', '#FF4040'],
     #         ['> 27', '#FF0000']]
    start_day = date(2012, 4,1)
    end_day = date(2012, 4,30)

    start_day1 = date(2012, 4,1)
    end_day1 = date(2012, 4,30)

    start_day2 = date(2012, 4,1)
    end_day2 = date(2012, 4,30)

 #   start_day = datetime(2012, 4,1)
  #  end_day = datetime(2012, 4,30)

    queryset = Meteocondition.objects.all()

#    qsstats = QuerySetStats(queryset, date_field='pr', aggregate=Count('id'))

    qsstats = QuerySetStats(queryset, date_field='curdate', aggregate_class=Max, aggregate_field='pr')

    values = qsstats.time_series(start_day, end_day, interval='days')

    values1 = qsstats.time_series(start_day1, end_day1, interval='days')

    values2 = qsstats.time_series(start_day2, end_day2, interval='days')



    va = []
    i = 0
    #for v in values:
     #   it = v[0]
      #  itt = v[1]*1000
       # va.append((it,itt))
#    import pdb; pdb.set_trace()
    return render_to_response('graph.html', {"values": values, "values1": values, "values2": values})





class graphic(HighChartsLineView):

    @property
    def series(self):
        categories = ['April', 'May', 'June']
        month = [4, 5, 6]
        result = []
        for name in categories:
            data = []
            for x in range(1,31):
                m = month[categories.index(name)]
                da = date(2012,m,x)
                qa = Meteocondition.objects.get(curdate=da)
                data.append(qa.pr)
            result.append({'name': name, "data": data})
        return result
    def get_data(self):
        data = super(graphic, self).get_data()
        data['xAxis'] = {}
        data['yAxis'] = self.y_axis
        data['yAxis']['title'] = {"text": 'Fire danger'}
        data['xAxis']['title'] = {"text": 'Days'}
        data['yAxis']['min'] = 0;
        data['yAxis']['max'] = 1;
        data['yAxis']['tickInterval'] = 0.2;
        data['xAxis']['allowDecimals'] = 0;
        return data


 #   return render_to_response('graphic.html', {"data": data, "values1": values, "values2": values})





class GraphTempJson(HighChartsLineView):

    @property
    def series(self):
        categories = ['April', 'May', 'June']
        month = [4, 5, 6]
        result = []
        for name in categories:
            data = []
            for x in range(1,31):
                m = month[categories.index(name)]
                da = date(2012,m,x)
                qa = Meteocondition.objects.get(curdate=da)
                data.append(qa.t)
            result.append({'name': name, "data": data})
        return result
    def get_data(self):
        data = super(GraphTempJson, self).get_data()
        data['xAxis'] = {}
        data['yAxis'] = self.y_axis
        data['yAxis']['title'] = {"text": 'Temperature'}
        data['xAxis']['title'] = {"text": 'Days'}
        return data


 #   return render_to_response('graphic.html', {"data": data, "values1": values, "values2": values})



class GraphPjcJson(HighChartsLineView):

    @property
    def series(self):
        categories = ['April', 'May', 'June']
        month = [4, 5, 6]
        result = []
        for name in categories:
            data = []
            for x in range(1,31):
                m = month[categories.index(name)]
                da = date(2012,m,x)
                qa = Meteocondition.objects.get(curdate=da)
                data.append(qa.pjc)
            result.append({'name': name, "data": data})
        return result
    def get_data(self):
        data = super(GraphPjcJson, self).get_data()
        data['xAxis'] = {}
        data['yAxis'] = self.y_axis
        data['yAxis']['title'] = {"text": 'Weather conditions probability'}
        data['xAxis']['title'] = {"text": 'Days'}
        data['yAxis']['min'] = 0;
        data['yAxis']['max'] = 1;
        data['yAxis']['tickInterval'] = 0.2;

        return data


 #   return render_to_response('graphic.html', {"data": data, "values1": values, "values2": values})




class GraphKmpJson(HighChartsLineView):

    @property
    def series(self):
        categories = ['April', 'May', 'June']
        month = [4, 5, 6]
        result = []
        for name in categories:
            data = []
            for x in range(1,31):
                m = month[categories.index(name)]
                da = date(2012,m,x)
                qa = Meteocondition.objects.get(curdate=da)
                data.append(qa.kmp)
            result.append({'name': name, "data": data})
        return result
    def get_data(self):
        data = super(GraphKmpJson, self).get_data()
        data['xAxis'] = {}
        data['yAxis'] = self.y_axis
        data['yAxis']['title'] = {"text": 'Nesterov index'}
        data['xAxis']['title'] = {"text": 'Days'}
        data['yAxis']['min'] = 0;

        data['yAxis']['tickInterval'] = 3000;
        return data


 #   return render_to_response('graphic.html', {"data": data, "values1": values, "values2": values})







class GetPolygonJson(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        if(self.request.GET['id']=='0'):
            return GeoPolygon.objects.all()
        else:
            return GeoPolygon.objects.all().filter(type_id=int(self.request.GET['id']))

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/'+self.request.GET['id']+'.txt'
        try:
            if(os.path.exists(cpath)):
                from django.http import HttpResponse
                f = open(cpath,'r')
                out = f.read()
                f.close()
                return HttpResponse(out, content_type="application/json")
        except:
            pass
        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response



class GetPolygonJsonFiredanger(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        return GeoPolygon.objects.all().filter(type_id=1)

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/firedanger.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response




class GetPolygonJsonFirerisk(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        return GeoPolygon.objects.all()

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/firerisk.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response



class GetPolygonJsonFirerisk1(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        return GeoPolygon.objects.all()

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/firerisk1.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response





class GetPolygonJsonFirerisk2(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        return GeoPolygon.objects.all()

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/firerisk2.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response





class GetPolygonJsonGraph(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    def get_queryset(self):
        return Meteocondition.objects.all()

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/graph.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response














class GetPolygonJsonFiredamage(GeoJSONLayerView):

    # Options
    precision = 4   # float
    simplify = 0.5  # generalization

    def get_queryset(self):
 #       return GeoPolygon.objects.all().filter(type_id=1)
        return GeoPolygon.objects.all()

    def render_to_response(self, context, **response_kwargs):
        from forestry.settings import PROJECT_PATH
        import os.path
        cpath = PROJECT_PATH+'/map_cache/firedamage.txt'
        if(os.path.exists(cpath)):
            from django.http import HttpResponse
            f = open(cpath,'r')
            out = f.read()
            f.close()
            return HttpResponse(out, content_type="application/json")

        """
        Returns a JSON response, transforming 'context' to make the payload.
        """
        serializer = GeoJSONSerializer()
        response = self.response_class(**response_kwargs)
        options = dict(properties=self.properties,
                       precision=self.precision,
                       simplify=self.simplify,
                       srid=self.srid,
                       geometry_field=self.geometry_field,
                       force2d=self.force2d)
        serializer.serialize(self.get_queryset(), stream=response, ensure_ascii=False,
                             **options)

        #import pdb; pdb.set_trace()
        f = open(cpath,'w')
        f.write(response.content)
        f.close()
        return response










#def graphic(request):
#    start_date = date(2005, 6, 1)
#    end_date=date(2005, 6, 30)
#    queryset = Meteocondition.objects.all()


#    qsstats = QuerySetStats(queryset, date_field='curdate', aggregate=Max('id'))
#    values = qsstats.time_series(start_date, end_date, interval='days')
#    return render_to_response('graphic.html', {'values':values})







class MapLayer(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization

class ForestLayer(GeoJSONLayerView):
    # Options
    precision = 4   # float
    simplify = 0.5  # generalization
    query_set =  GeoPolygon.objects.all()

from django.conf.urls import patterns, include, url
from djgeojson.views import GeoJSONLayerView
from map.views import GetPolygonJson, MapLayer, ForestLayer, GetPolygonJsonFiredanger, GetPolygonJsonFiredamage, GetPolygonJsonFirerisk, GetPolygonJsonFirerisk1, GetPolygonJsonFirerisk2, GetPolygonJsonGraph
from map.models import GeoKvartal, GeoPolygon, Meteocondition
from django.views.decorators.cache import cache_page
# Uncomment the next two lines to enable the admin:
from django.conf import settings
from django.conf.urls.static import static
from map.views import graphic, GraphTempJson, GraphKmpJson, GraphPjcJson

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',

    url(r'^firerisk/', include('firerisk.urls')),
    url(r'^graphic$', graphic.as_view(), name="graphic"),

    url(r'^GraphTempJson$', GraphTempJson.as_view(), name="GraphTempJson"),
    url(r'^GraphKmpJson$', GraphKmpJson.as_view(), name="GraphKmpJson"),
    url(r'^GraphPjcJson$', GraphPjcJson.as_view(), name="GraphPjcJson"),



    url(r'^$', 'main.views.welcome',name="homepage"),

    url(r'^regions$', 'map.views.mapregion', name="map-region"),
    url(r'^fire-danger$', 'map.views.firedanger', name="fire-danger"),
    url(r'^fire-damage$', 'map.views.firedamage', name="fire-damage"),
    url(r'^fire-risk$', 'map.views.firerisk', name="fire-risk"),
    url(r'^fire-risk1$', 'map.views.firerisk1', name="fire-risk1"),
    url(r'^fire-risk2$', 'map.views.firerisk2', name="fire-risk2"),

    url(r'^graph$', 'map.views.graph', name="graph"),









    # url(r'^$', 'forestry.views.home', name='home'),
    # url(r'^forestry/', include('forestry.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^data.geojson$', GeoJSONLayerView.as_view(model=GeoKvartal), name='data'),
    url(r'^grappelli/', include('grappelli.urls')), # grappelli URLS

    url(r'^map/', include('map.urls')),


    url(ur'^get-polygon-json$',  GetPolygonJson.as_view(model=GeoPolygon,properties=('id',)), name='get-poligon-json'),

    url(ur'^get-polygon-json-firedanger$',  GetPolygonJsonFiredanger.as_view(model=GeoPolygon,properties=('id','firerisk',)), name='get-poligon-json-firedanger'),
    url(ur'^get-polygon-json-firedamage$',  GetPolygonJsonFiredamage.as_view(model=GeoPolygon,properties=('id','class_damage',)), name='get-poligon-json-firedamage'),

    url(ur'^get-polygon-json-firerisk$',  GetPolygonJsonFirerisk.as_view(model=GeoPolygon,properties=('id','class_risk',)), name='get-poligon-json-firerisk'),
    url(ur'^get-polygon-json-firerisk1$',  GetPolygonJsonFirerisk1.as_view(model=GeoPolygon,properties=('id','class_risk1',)), name='get-poligon-json-firerisk1'),
    url(ur'^get-polygon-json-firerisk2$',  GetPolygonJsonFirerisk2.as_view(model=GeoPolygon,properties=('id','class_risk2',)), name='get-poligon-json-firerisk2'),

    url(ur'^get-polygon-json-graph$',  GetPolygonJsonGraph.as_view(model=Meteocondition,properties=('id','pr',)), name='get-poligon-json-graph'),







    url(r'^admin/', include(admin.site.urls)),
    url(r'^mushrooms/geojson$', MapLayer.as_view(model=GeoKvartal,properties=('number','id')), name='mushrooms'),
    url(r'^forest$', ForestLayer.as_view(model=GeoPolygon), name='mushrooms'),

    )+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)



